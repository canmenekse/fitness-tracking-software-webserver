<?php
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/DatabaseHandler.php');
    require_once($root.'/Webserver/Requesters/Sign Up/Trainer/Insert/CreateTrainerAccountRequester.php');
    require_once($root.'/Webserver/Requesters/Sign Up/Check/CheckUserExistRequester.php');
    require_once($root.'/Webserver/Utilities/utilities.php');
    session_start();
    $messageArray=array();
    //A logged in user should not attempt to create an account
    
        $databaseHandler=new DatabaseHandler();
        $createAccountRequester=new CreateTrainerAccountRequester($databaseHandler);
        //set the $_SESSION
        $allow=false;
        //to verify whether same name exists
        $checkUserExistRequester=new CheckUserExistRequester($databaseHandler);
        
        //check whether we have the necessary parameters(we do not check the parameters of the checkUserEquist Requester here)
       if($createAccountRequester->hasRequiredAccessLevel()==true)
        {       
        
            if($createAccountRequester->hasNecessaryParameters()==true)
            {
                $_SESSION['role']="trainer";
                //Check whether user already exist
                $query=$checkUserExistRequester->createRequest();
                $checkUserExistRequester->sendRequest($query);
              
                $queryResult=$checkUserExistRequester->getQueryResult();
                if(mysqli_num_rows($queryResult)==1)
                {
                     JSONMessageAddToArray($messageArray,"Error","Username or email is in use");
                }
                else
                {
                 //Add the user
                 
                 $query=$createAccountRequester->createRequest();
                 
                 $createAccountRequester->sendRequest($query);
                 JSONMessageAddToArray($messageArray,"Notice","Account created");
                }
            }
            else
            {
                $temp=sprintf( " Username is %s, Password is %s, Email is %s, Phone is %s",$_POST['username'],$_POST['password'],$_POST['email'],$_POST['phone']); 
                print $temp;
                 JSONMessageAddToArray($messageArray,"Error","Please use the form");
            }
        }
        else
        {
            JSONMessageAddToArray($messageArray,"Error","You are already have an account");
        }
   
   JSONMessageOutputter($messageArray);
  
?>