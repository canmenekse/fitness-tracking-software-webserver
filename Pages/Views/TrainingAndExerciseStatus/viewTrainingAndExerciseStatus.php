<?php
  $root=$_SERVER['DOCUMENT_ROOT'];
  require_once($root.'/Webserver/DatabaseHandler.php');
  require_once($root.'/Webserver/LoginManager.php');
  require_once($root.'/Webserver/Requesters/Views/ViewTrainingProgramsAndExercisesStatusRequester.php');
 
  session_start();
  $databaseHandler=new DatabaseHandler();
  $messageArray=array();
  //Generate a Request to check the user
  $viewRequester=new ViewTrainingProgramsAndExercisesStatusRequester($databaseHandler);
  //Checks accessLevel
  if($viewRequester->hasRequiredAccessLevel()==true)
  {
    if($viewRequester->hasNecessaryParameters()==true)
    {
    //Create the request
    $query=$viewRequester->createRequest();
    //print $query;
    $viewRequester->sendRequest($query);
    $queryResult=$viewRequester->getQueryResult();
    $json_output = array();
    
    //Get the query and put them in array
    while( $row=mysqli_fetch_assoc($queryResult))
        {
            
            
            $json_output[] = json_encode($row);
            
           
           
        }
        //var_dump($json_output);
    }
    
    else
    {
        JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary parameters");
    }
  
  
  }
  
  else
  {
    JSONMessageAddToArray($messageArray,"Error","You do not have the necessary permissions");
  }
    JSONMessageOutputter($json_output);




?>