<?php
  $root=$_SERVER['DOCUMENT_ROOT'];
  require_once($root.'/Webserver/DatabaseHandler.php');
  require_once($root.'/Webserver/LoginManager.php');
  require_once($root.'/Webserver/Requesters/Views/ViewAchievementsRequester.php');
 
  session_start();
  $databaseHandler=new DatabaseHandler();
  $messageArray=array();
  //Generate a Request to check the user
  $viewAchievementRequester=new ViewAchievementsRequester($databaseHandler);
  //Checks accessLevel
  if($viewAchievementRequester->hasRequiredAccessLevel()==true)
  {
    if($viewAchievementRequester->hasNecessaryParameters()==true)
    {
    //Create the request
    $query=$viewAchievementRequester->createRequest();
    $viewAchievementRequester->sendRequest($query);
    $queryResult=$viewAchievementRequester->getQueryResult();
    //Get the query and put them in array
    $i=0;
    while( $row=mysqli_fetch_assoc($queryResult))
        {
            $i++;
           
           JSONMessageAddtoArray($messageArray,$row['Achievement_ID'],$row['Achievement_Name']);
        }
    }
    
    else
    {
        JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary parameters");
    }
  
  
  }
  
  else
  {
    JSONMessageAddToArray($messageArray,"Error","You do not have the necessary permissions");
  }
    JSONMessageOutputter($messageArray);





?>