<?php
  $root=$_SERVER['DOCUMENT_ROOT'];
  require_once($root.'/Webserver/DatabaseHandler.php');
  require_once($root.'/Webserver/Requesters/Views/ViewFinishedTrainingsRequester.php');
 
  session_start();
  $databaseHandler=new DatabaseHandler();
  $messageArray=array();
  //Generate a Request to check the user
  $viewFinishedTrainingsRequester=new ViewFinishedTrainingsRequester($databaseHandler);
  //Checks accessLevel
  if($viewFinishedTrainingsRequester->hasRequiredAccessLevel()==true)
  {
    if($viewFinishedTrainingsRequester->hasNecessaryParameters()==true)
    {
    //Create the request
    $query=$viewFinishedTrainingsRequester->createRequest();
    $viewFinishedTrainingsRequester->sendRequest($query);
    $queryResult=$viewFinishedTrainingsRequester->getQueryResult();
    //Get the query and put them in array
    $i=0;
    while( $row=mysqli_fetch_assoc($queryResult))
        {
            $i++;
           
           JSONMessageAddtoArray($messageArray,$row['Training_Program_ID'],$row['Training_Program_Name']);
        }
    }
    
    else
    {
        JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary parameters");
    }
  
  
  }
  
  else
  {
    JSONMessageAddToArray($messageArray,"Error","You do not have the necessary permissions");
  }
    JSONMessageOutputter($messageArray);





?>