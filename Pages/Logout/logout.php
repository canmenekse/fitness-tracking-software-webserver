<?php
   
    $root=$_SERVER['DOCUMENT_ROOT'];
     session_start();
    require_once($root.'/Webserver/Utilities/utilities.php');
    unset($_SESSION['DatabaseHandler']);
    if(isset($_SESSION['access'])!=false)
    {
        $access=$_SESSION['access'];
        session_destroy();
        if($access==2)
        {
            waitAndRedirect('Webserver/Admin Panel/adminLogin.php',2);
        }
        
    }    
?>