<?php
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/DatabaseHandler.php');
    require_once($root.'/Webserver/Utilities/utilities.php');
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver/Requesters/Exercises/Insert/CreateExerciseRequester.php');
    require_once($root.'/Webserver/Table Informations/Info/Info.php');
    require_once ($root.'/Webserver/Utilities/ConvertNameToID.php');
    session_start();
    $info=Info::getInstance();
   
    $messageArray=array();
    $databaseHandler=new DatabaseHandler();
    $createExerciseRequester=new CreateExerciseRequester($databaseHandler);
    
    
    
    
   
    if($createExerciseRequester->hasRequiredAccessLevel())
    {
        
        if($createExerciseRequester->hasNecessaryParameters())
        {
            
            $name=$_POST['name'];
            $converter=new ConvertNameToID($databaseHandler);
            $exercisesInfo=$info->getExercisesTableInfo();
            $id=$converter->convertToId($name,$exercisesInfo->getTableName(),$exercisesInfo->getID(),$exercisesInfo->getName());
            
            
            
             if($id==-1)
            {
                
               $query=$createExerciseRequester->createRequest();
               $createExerciseRequester->sendRequest($query);
               
              JSONMessageAddtoArray($messageArray,"Success","created Exercise ");
              
            }
            else
            {
                JSONMessageAddtoArray($messageArray,"Error","Duplicate Exercise Name ");
            }
        }
        else
        {
            JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary parameters ");
        
        }
    
    
    }
    
    else
    {
        JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary permissions");
       
    }
    
     JSONMessageOutputter($messageArray);
     


?>  
  
  
  
  
  
  
  
