<?php
  $root=$_SERVER['DOCUMENT_ROOT'];
  require_once($root.'/Webserver/DatabaseHandler.php');
  require_once($root.'/Webserver/LoginManager.php');
  require_once($root.'/Webserver/Requesters/Task/Check/CheckTaskExistsRequester.php');
  require_once($root.'/Webserver/Requesters/Task/Update/UpdateTaskRequester.php');
  require_once($root.'/Webserver/Requesters/Task/Insert/InsertTaskRequester.php');
  require_once($root.'/Webserver/Requesters/Achievements/Check/DiscoverFinishedAchievementsRequester.php');
  require_once($root.'/Webserver/Requesters/Achievements/Insert/InsertCompletedAchievementRequester.php');
  require_once($root.'/Webserver/Requesters/Training Programs/Check/DiscoverFinishedTrainingsRequester.php');
  require_once($root.'/Webserver/Requesters/Training Programs/Update/UpdateTrainingToCompletedRequester.php');
  require_once($root.'/Webserver/Requesters/Views/GetBonusOfAchievementRequester.php');
  require_once($root.'/Webserver/Requesters/Achievements/Content/Update/UpdateBonusRequester.php');

 

 
 
 
  function modifyContents($discover,$insert,$sessionName,$databaseFieldName,$databaseHandler)
{
    
        
        $query=$discover->createRequest();
         
        $discover->sendRequest($query);
        $queryResult=$discover->getQueryResult();
        $temparray=array();
        while($row=mysqli_fetch_assoc($queryResult))
        {
            
           $temparray[]=$row[$databaseFieldName];
        }
       
        $getBonusOfAchievementRequester= new GetBonusOfAchievementRequester($databaseHandler);
        $updateBonusRequester=new UpdateBonusRequester($databaseHandler);
        
        foreach($temparray as $ID)
        {
            $_SESSION[$sessionName]=$ID;
            if($sessionName=="achievement_ID")
            {
                    
                     
                    if($getBonusOfAchievementRequester->hasNecessaryParameters())
                    {
                        $query=$getBonusOfAchievementRequester->createRequest();
                       
                        $getBonusOfAchievementRequester->sendRequest($query);
                        
                        $queryResult=$getBonusOfAchievementRequester->getQueryResult();
                        if( $row = mysqli_fetch_assoc($queryResult))
                        {
                            $bonus= $row['Achievement_Bonus'];
                            $_SESSION['Bonus']=$bonus;
                          
                        }
                        $query=$updateBonusRequester->createRequest();
                        
                        $updateBonusRequester->sendRequest($query);
                    }
                    
                   
                    
                    
            }
            
            if($insert->hasNecessaryParameters()==true)
            {
            $query=$insert->createRequest();
            $insert->sendRequest($query);
            }
           
        }
 
} 
 
  session_start();
 
   $databaseHandler=new DatabaseHandler();
  $messageArray=array();
  //Generate a Request to check the user
  $checkTaskExistsRequester=new CheckTaskExistsRequester($databaseHandler);
  //Checks accessLevel
 
  if($checkTaskExistsRequester->hasRequiredAccessLevel()==true)
  {
    
    if($checkTaskExistsRequester->hasNecessaryParameters()==true)
    {
       $query=$checkTaskExistsRequester->createRequest();
       $checkTaskExistsRequester->sendRequest($query);
      //print " checkTaskExists query is: ". $query;
       $queryResult=$checkTaskExistsRequester->getQueryResult();
       //Update
       if(mysqli_num_rows($queryResult)==1)
        {
            $updateTaskRequester=new UpdateTaskRequester($databaseHandler);
            if($updateTaskRequester->hasRequiredAccessLevel()==true)
            {
                if($updateTaskRequester->hasNecessaryParameters()==true)
                {
                    $query=$updateTaskRequester->createRequest();
                    //print " UpdateTaskRequester query is: ". $query;
                    $updateTaskRequester->sendRequest($query);
                    $queryResult=$updateTaskRequester->getQueryResult();
                    //print " updated" ;
                }
                else
                {
                    JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary parameters");
                }
            }
            else
            {
             JSONMessageAddToArray($messageArray,"Error","You do not have the necessary permissions");
            }
           
        }
        //Insert
        else
        {
            $insertTaskRequester=new InsertTaskRequester($databaseHandler);
            if($insertTaskRequester->hasRequiredAccessLevel()==true)
            {
                if($insertTaskRequester->hasNecessaryParameters()==true)
                {
                    $query=$insertTaskRequester->createRequest();
                    //print $query;
                    $insertTaskRequester->sendRequest($query);
                    $queryResult=$insertTaskRequester->getQueryResult();
                    //print "inserted" ;
                }
                else
                {
                    JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary parameters");
                }
            }
            else
            {
             JSONMessageAddToArray($messageArray,"Error","You do not have the necessary permissions");
            }
          
            
        }
          $discover=new DiscoverFinishedAchievementsRequester($databaseHandler);
          $insert=new InsertCompletedAchievementRequester($databaseHandler);
         // print " Discover achievement query is: ". $query;
          modifyContents($discover,$insert,"achievement_ID","Achievement_ID",$databaseHandler);
         unset($_SESSION["achievement_ID"]);

         $discover=new DiscoverFinishedTrainingsRequester($databaseHandler);
         $update=new UpdateTrainingToCompletedRequester($databaseHandler);
          modifyContents($discover,$update,"training_Program_ID","Training_Program_ID",$databaseHandler);
           unset($_SESSION['training_Program_ID']);
          
    }
    
    else
    {
        JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary parameters");
    }
  
  
  }
  
  else
  {
    JSONMessageAddToArray($messageArray,"Error","You do not have the necessary permissions");
     print " Please log in correctly:";
  }
   // JSONMessageOutputter($messageArray);
    




?>