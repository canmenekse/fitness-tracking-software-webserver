<?php
     
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/DatabaseHandler.php');
    require_once($root.'/Webserver/LoginManager.php');
    require_once($root.'/Webserver/Requesters/Login/Check/CheckUserLoginRequester.php');
    session_start();
    //start the session
   
    //User is maybe not logged in or maybe he tried to logged in but get the accessLevel -1
        //Create the databaseHandler
        $databaseHandler=new DatabaseHandler();
        $messageArray=array();
        //Generate a Request to check the user
        $checkUserLoginRequester=new CheckUserLoginRequester($databaseHandler);
        //Create a login manager with the checkUserLoginRequester
        $LoginManager=new LoginManager($checkUserLoginRequester);
        //Check whether it has the necessary parameters
        if($LoginManager->getCheckUserLoginRequester()->hasRequiredAccessLevel()==true)
        {
        //Get the role
            if($LoginManager->getcheckUserLoginRequester()->hasNecessaryParameters()==true)
            {
            $role=$_POST['role'];
            $_SESSION['role']=$role;
            //Send to Login Manager to manage
            $LoggedIn=$LoginManager->Manage($role);
                if($LoggedIn==true)
                {
                    JSONMessageAddToArray($messageArray,"Notice","You are logged in");
                    JSONMessageAddToArray($messageArray,"ID",$_SESSION['ID']);
                    
                }
                else
                {
                    if($role=="trainer")
                    {
                     JSONMessageAddToArray($messageArray,"Error","You may entered incorrect credentials or your account has not been approved yet ");
                    }
                    else
                    {
                    JSONMessageAddToArray($messageArray,"Error","You  entered invalid credentials");
                    }
                }
            
            }
            else
            {
            JSONMessageAddToArray($messageArray,"Error","Please use the form");
            }
        
        }
        else
        {
          JSONMessageAddToArray($messageArray,"Notice","You are already Logged In");
          JSONMessageAddToArray($messageArray,"ID",$_SESSION['ID']);
            if($_SESSION['access']==2)
            {
               
               waitAndRedirect('Webserver/Admin Panel/trainerOperations.php',2);
            }
          
        }
      
    JSONMessageOutputter($messageArray);
    
?>