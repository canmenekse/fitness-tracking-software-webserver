<?php
    //$root.'/Webserver/
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/DatabaseHandler.php');
    require_once($root.'/Webserver/Utilities/utilities.php');
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver/Requesters/Training Programs/Assign/AssignTrainingProgramToTraineeRequester.php');
     require_once($root.'/Webserver/Table Informations/Info/Info.php');
    require_once ($root.'/Webserver/Utilities/ConvertNameToID.php');
    require_once($root.'/Webserver/Requesters/Task/Insert/insertEmptyTaskRequester.php');
    require_once($root.'/Webserver/Requesters/Views/viewExercisesOfTheTrainingRequester.php');
    require_once($root.'/Webserver/Requesters/Training Programs/Assign/Check/CheckPreviouslyAssignedTrainingProgramRequester.php');
    session_start();
    $databaseHandler=new DatabaseHandler();
    $assignTrainingProgramToTraineeRequester= new AssignTrainingProgramToTraineeRequester($databaseHandler);
    $name=$_POST['trainee_Name'];
   // print " Post Training Program ID is " . $_POST['training_Program_ID'];
    $_SESSION['training_Program_ID']=$_POST['training_Program_ID'];
    $converter=new ConvertNameToID($databaseHandler);
     $info=Info::getInstance();
     $traineeInfo=$info->getTraineeTableInfo();
     $trainee_ID=$converter->convertToId($name,$traineeInfo->getTableName(),$traineeInfo->getID(),$traineeInfo->getName());
     //print "Trainee ID is ". $trainee_ID;
     $messageArray = array();
    if($trainee_ID!=-1)
    {
        
        $_SESSION['trainee_ID']=$trainee_ID;
        
        if($assignTrainingProgramToTraineeRequester->hasRequiredAccessLevel())
        {
                
            if($assignTrainingProgramToTraineeRequester->hasNecessaryParameters())
            {
                
                $checkPreviouslyAssignedTrainingProgramRequester=new CheckPreviouslyAssignedTrainingProgramRequester($databaseHandler);
                $query=$checkPreviouslyAssignedTrainingProgramRequester->createRequest();
                $checkPreviouslyAssignedTrainingProgramRequester->sendRequest($query);
                //print $query;
                $queryResult=$checkPreviouslyAssignedTrainingProgramRequester->getQueryResult();
                if($row=mysqli_fetch_assoc($queryResult))
                {
                    JSONMessageAddtoArray($messageArray,"Error","You cannot assign the same program");
                }
                else
                {
                
                 
                  addEmptyTasks($databaseHandler);
                  $query=$assignTrainingProgramToTraineeRequester->createRequest();
                  $assignTrainingProgramToTraineeRequester->sendRequest($query);
                  JSONMessageAddtoArray($messageArray,"Success","Assigned Training Program");
                   //unset( $_SESSION['training_Program_ID']);
                }
                 
        
            }
                
            
            
            else
            {
                JSONMessageAddtoArray($messageArray,"Warning","You do not have the necessary parameters");
                
            }
        
        }
        else
        {
            JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary permissions");
        }
    }
        
    else
    {
        JSONMessageAddtoArray($messageArray,"Error","No such trainee exists");
            
            
    }
        
         
          
      
    JSONMessageOutputter($messageArray);    
     
      
      unset( $_SESSION['trainee_ID']);

    function addEmptyTasks($databaseHandler)
    {
     $viewExercisesOfTheTrainingRequester=new viewExercisesOfTheTrainingRequester($databaseHandler);
     $query=$viewExercisesOfTheTrainingRequester->createRequest();
     $viewExercisesOfTheTrainingRequester->sendRequest($query);
     $queryResult=$viewExercisesOfTheTrainingRequester->getQueryResult();
     $_SESSION['amount']=0;
     $insertEmptyTaskRequester=new InsertEmptyTaskRequester($databaseHandler);
     while ($row= mysqli_fetch_assoc($queryResult))
     {
            $_SESSION['exercise_ID']=$row['Exercise_ID'];
            $query=$insertEmptyTaskRequester->createRequest();
            $insertEmptyTaskRequester->sendRequest($query);
     }
     
    
    }
?>  
  
  
  
  
  
  
  
