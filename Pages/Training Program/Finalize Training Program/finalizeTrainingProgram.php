<?php

    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/DatabaseHandler.php');
    require_once($root.'/Webserver/Utilities/utilities.php');
    require_once($root.'/Webserver/Requesters/Training Programs/Update/FinalizeTrainingProgramRequester.php');
    session_start();
    $messageArray=array();
    $databaseHandler=new DatabaseHandler();
    $finalizeTrainingProgramRequester= new FinalizeTrainingProgramRequester($databaseHandler);
    if($finalizeTrainingProgramRequester->hasRequiredAccessLevel()==true)
    {
        if($finalizeTrainingProgramRequester->hasNecessaryParameters()==true)
        {
            $query=$finalizeTrainingProgramRequester->createRequest();
            $finalizeTrainingProgramRequester->sendRequest($query);
            JSONMessageAddToArray($messageArray,"Notice","Training Program is finalized.");
        }
        else
        {
            JSONMessageAddToArray($messageArray,"Error","You do not have the necessary parameters");
        }
    }
    else
    {
        JSONMessageAddToArray($messageArray,"Error","You do not have the necessary permissions");
    }
    JSONMessageOutputter($messageArray);
    unset($_SESSION['training_Program_ID']);





?>