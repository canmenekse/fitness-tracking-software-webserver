<?php
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/DatabaseHandler.php');
    require_once($root.'/Webserver/Utilities/utilities.php');
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver/Requesters/Training Programs/Insert/CreateTrainingProgramRequester.php');
    require_once($root.'/Webserver/Requesters/Training Programs/Check/CheckTrainingProgramExistsRequester.php');
    session_start();
    
    $messageArray=array();
    $databaseHandler=new DatabaseHandler();
    $createTrainingProgramRequester=new CreateTrainingProgramRequester($databaseHandler);
    
    
    
    
   
    if($createTrainingProgramRequester->hasRequiredAccessLevel())
    {
        
        if($createTrainingProgramRequester->hasNecessaryParameters())
        {
            
            
            $checkTrainingProgramExistsRequester=new CheckTrainingProgramExistsRequester($databaseHandler);
            $query=$checkTrainingProgramExistsRequester->createRequest();
            
            
            $checkTrainingProgramExistsRequester->sendRequest($query);
            $queryResult=$checkTrainingProgramExistsRequester->getQueryResult();
             if(mysqli_num_rows($queryResult)!=1)
            {
                $createTrainingProgramRequester= new createTrainingProgramRequester($databaseHandler);
               $query=$createTrainingProgramRequester->createRequest();
               $createTrainingProgramRequester->sendRequest($query);
               $queryResult=$createTrainingProgramRequester->getQueryResult();
              JSONMessageAddtoArray($messageArray,"Success","created training program ");
              
            }
            else
            {
                JSONMessageAddtoArray($messageArray,"Error","Duplicate Training Program ");
            }
        }
        else
        {
            JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary parameters ");
        
        }
    
    
    }
    
    else
    {
        JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary permissions");
       
    }
    
     JSONMessageOutputter($messageArray);
      
      //
     
      


?>  
  
  
  
  
  
  
  
