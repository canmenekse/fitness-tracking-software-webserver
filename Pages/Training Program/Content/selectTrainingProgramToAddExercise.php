<?php
  $root=$_SERVER['DOCUMENT_ROOT'];
  require_once($root.'/Webserver/Utilities/utilities.php');
  require_once($root.'/Webserver/DatabaseHandler.php');
  require_once($root.'/Webserver/LoginManager.php');
  require_once($root.'/Webserver/Requesters/Views/ViewAddableTrainingProgramsRequester.php');
   session_start();
  if(validVariable($_POST['training_Program_ID'])==true)
  {
    $_SESSION['training_Program_ID']=$_POST['training_Program_ID'];
  }
 
  $databaseHandler=new DatabaseHandler();
  $messageArray=array();
  //Generate a Request to check the user
  $viewAddableTrainingsRequester=new ViewAddableTrainingProgramsRequester($databaseHandler);
  //Checks accessLevel
  if($viewAddableTrainingsRequester->hasRequiredAccessLevel()==true)
  {
    if($viewAddableTrainingsRequester->hasNecessaryParameters()==true)
    {
    //Create the request
    $query=$viewAddableTrainingsRequester->createRequest();
   
    $viewAddableTrainingsRequester->sendRequest($query);
    $queryResult=$viewAddableTrainingsRequester->getQueryResult();
    //Get the query and put them in array
    $i=0;
    while( $row=mysqli_fetch_assoc($queryResult))
        {
            $i++;
           
           JSONMessageAddtoArray($messageArray,$row['Training_Program_Name'],$row['Training_Program_ID']);
        }
    }
    
    else
    {
        JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary parameters");
    }
  
  
  }
  
  else
  {
    JSONMessageAddToArray($messageArray,"Error","You do not have the necessary permissions");
  }
    JSONMessageOutputter($messageArray);





?>