<?php
    //$root.'/Webserver/
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/DatabaseHandler.php');
    require_once($root.'/Webserver/Utilities/utilities.php');
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver/Requesters/Training Programs/Content/Insert/InsertTrainingProgramContentRequester.php');
    require_once($root.'/Webserver/Requesters/Training Programs/Content/Check/CheckExerciseExistsInTrainingProgramContentRequester.php');
    require_once($root.'/Webserver/Requesters/Training Programs/Content/Update/UpdateTrainingProgramContentRequester.php');
    session_start();
    $databaseHandler=new DatabaseHandler();
    $checkExerciseExistsInTrainingProgramContentRequester=new CheckExerciseExistsInTrainingProgramContentRequester($databaseHandler);
    
    
    
    $messageArray = array();
    
    if($checkExerciseExistsInTrainingProgramContentRequester->hasRequiredAccessLevel())
    {
            
        if($checkExerciseExistsInTrainingProgramContentRequester->hasNecessaryParameters())
        {
            
            
            
            $query=$checkExerciseExistsInTrainingProgramContentRequester->createRequest();
            
            
            
            $checkExerciseExistsInTrainingProgramContentRequester->sendRequest($query);
            $queryResult=$checkExerciseExistsInTrainingProgramContentRequester->getQueryResult();
             if(mysqli_num_rows($queryResult)!=1)
            {
               $insertTrainingProgramContentRequester=new InsertTrainingProgramContentRequester($databaseHandler);
               $query=$insertTrainingProgramContentRequester->createRequest();
               $insertTrainingProgramContentRequester->sendRequest($query);
			   JSONMessageAddtoArray($messageArray,"Success","Added Training Program Content ");
			   
             
    
            }
            else
            {
                $updateTrainingProgramContentRequester=new UpdateTrainingProgramContentRequester($databaseHandler);
                
                $query=$updateTrainingProgramContentRequester->createRequest();
                $updateTrainingProgramContentRequester->sendRequest($query);
			JSONMessageAddtoArray($messageArray,"Success","Updated Training Program Content ");
                
                    
                
                
            }
        }
        else
        {
			JSONMessageAddtoArray($messageArray,"Warning","You do not have the necessary parameters");
            //waitAndRedirect("addTrainingProgram.php",2);
        }
    
    
    }
    
    else
    {
		
        JSONMessageAddtoArray($messageArray,"Warning","You are not logged in");
        //waitAndRedirect("adminPanel.php",2);
    }
    
     
      
      
    JSONMessageOutputter($messageArray);    
      unset( $_SESSION['training_Program_ID']);


?>  
  
  
  
  
  
  
  
