<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Utilities/utilities.php'); 

class LoginManager
{
    //Data members
    private $checkUserLoginRequester;
    private $accessLevel;
    private $ID;
    /*
    Constructor
    Precondition:The parameters must be initialized before.
    Postcondition:LoginManager is created.
    */
    //Methods
    function __construct($checkUserLoginRequester)
    {
        $this->checkUserLoginRequester=$checkUserLoginRequester;
    }
    
    /*
    Precondition:User enters a valid login parameters
    Postcondition:Sets the accessLevel and the UserId
    */
    public function manage($role)
    {
        
        //Query to create a checklogin query
        $query=$this->checkUserLoginRequester->createRequest();
      
        //Send a request with using checkRequester
        $this->checkUserLoginRequester->sendRequest($query);
      
        
        //Get the result of the query
        $queryResult=$this->checkUserLoginRequester->getQueryResult();
        
        $num_rows = $queryResult->num_rows;
        //print "number of rows is ". $num_rows;
        //get the number of rows returned when it is 1 then it is a valid login
        if(mysqli_num_rows($queryResult)==1)
        {
            
            $this->setAccessLevel($this->discoverAccessLevel($role));
            $this->setUserId();
            if($_SESSION['access']==2)
            {
                $root=$_SERVER['DOCUMENT_ROOT'];
                waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",5);
            }
            return true;
        }
        else
        {
            //Do not accept the login.
            $this->setAccessLevel(-1);
            return false;
            
        }
        
    }
    /*
    Precondition:Session must be started
    Postcondition:AccessLevel is set
    */
    public function setAccessLevel($accessLevel)
    {
        $_SESSION['access']=$accessLevel;
    
    }
    /*
    Precondition:Session must be started
    Postcondition:ID is set
    */
    public function setUserId()
    {
        $_SESSION['ID']=$this->getUserId();
       
    }
    /*
    Precondition:User enters a login information that is in the database
    Postcondition: returns the id of the user
    */
    public function getUserId()
    {
        $query=$this->checkUserLoginRequester->createGetIdRequest();
        $this->checkUserLoginRequester->sendRequest($query);
        $queryResult=$this->checkUserLoginRequester->getQueryResult();
        if($row=$queryResult->fetch_assoc())
        {
            return $row['ID'];
        }
    }
     /*
    Precondition:checkUserLoginRequester must be initialized
    Postcondition: returns the checkUserLoginRequester
    */
    public function getCheckUserLoginRequester()
    {
        return $this->checkUserLoginRequester;
    }
    public function discoverAccessLevel($role)
    {
        $hashes = ["trainee" => 0,"trainer" => 1,"admin"=>2];
        return $hashes[$role];
    }
    


}
?>