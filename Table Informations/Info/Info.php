<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Table Informations/TrainerTableInfo.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Table Informations/TraineeTableInfo.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Table Informations/ExerciseTableInfo.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Table Informations/AchievementTableInfo.php');
    class Info
    {
        
        
        private $traineeTableInfo;
        private $trainerTableInfo;
        private $exercisesTableInfo;
        private $achievementsTableInfo;
        
        public static function getInstance()
        {
           
            if(isset($_SESSION['INFO'])==false)
            {
                
                $_SESSION['INFO']=new Info();
            }
            return $_SESSION['INFO'];
        
        }
        private function __construct()
        {
            
            $this->traineeTableInfo=new TraineeTableInfo();
            $this->trainerTableInfo=new TrainerTableInfo();
            $this->exercisesTableInfo=new ExerciseTableInfo();
            $this->achievementsTableInfo=new AchievementTableInfo();
        }
        public function getTraineeTableInfo()
        {
            return $this->traineeTableInfo;
        }
        public function getTrainerTableInfo()
        {
            return $this->trainerTableInfo;
        }
        public function getExercisesTableInfo()
        {
            return $this->exercisesTableInfo;
        }
        public function getAchievementsTableInfo()
        {
            return $this->achievementsTableInfo;
        }
    
    }


?>