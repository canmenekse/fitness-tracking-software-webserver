<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Table Informations/TableInfo.php');
    class TrainingProgramTableInfo extends TableInfo
    {
     
         function __construct()
         {
             $this->ID_FIELD="Training_Program_ID";
             $this->NAME_FIELD="Training_Program_Name";
             $this->TABLE_NAME="training_program";
         }
     }
     
?>      
  