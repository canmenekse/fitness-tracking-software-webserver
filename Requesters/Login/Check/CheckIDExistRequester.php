<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    //CheckID Requester
    class CheckIDExistRequester extends Requester
    {
        
        //Methods
       
        function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
        /*
        Postcondition:returns the table name
        */
        public function getTableName($role)
        {
            if($role=="trainer"||$role=="trainee")
            {
              //'trainer' become 'trainers','trainee' become 'trainees'
              $tableName=$role."s";
              
            }
            else
            {
             $tableName="";
            }
           return $tableName;
        }
        /*
        Precondition:Has the necessary parameters
        Postcondition:Returns a query
        */
        public function createRequest()
        {
           //Get the parameters
           $role=$_SESSION['role'];
           $ID=$_SESSION['sentID'];
          
           //get the table name
           $tableName=$this->getTableName($role);
           //Dont generate query with invalid table name
           $Query=sprintf("SELECT 1 from %s WHERE ID=%d",
           mysql_real_escape_string($tableName),
           mysql_real_escape_string($ID)
           );
         return $Query;
         }
        
        /*
        Postcondition:Returns true when has Required access and false otherwise
        */
        public function hasRequiredAccessLevel()
        {
           if(isset($_SESSION['access'])==false||$_SESSION['access']>0)
           {
            return true;
           }
           else
           {
            return false;
           }
        }
        /*
        Postcondition:Returns true when has the necessary parameters
        */
        public function hasNecessaryParameters()
        {
            //Check whether the necessary parameters are set and non empty
            if(isset($_SESSION['sentID'])==false||empty($SESSION['sentID'])==true)
            {
                print "parameters not set ";
                
                return false;
            }
            
            return true;
        }
        
       
    }
?>