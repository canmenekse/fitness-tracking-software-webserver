<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    //CheckUser Requester
    class CheckUserLoginRequester extends Requester
    {
        
        //Methods
       
        function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
        public function getTableName($role)
        {
             
            if($role=="trainer"||$role=="trainee")
            {
              //'trainer' become 'trainers','trainee' become 'trainees'
              $tableName=$role."s";
              //print "Entered here";
            }
            else if ($role=="admin")
            {
               
                $tableName=$role;
            }
            else
            {
             $tableName="";
            }
           return $tableName;
        }
        /*
        Precondition:Has the necessary parameters
        Postcondition:Returns a query
        */
        public function createRequest()
        {
        
           //Get the parameters
           $username= $_POST['username'];
           $password= $_POST['password'];
           $role= $_POST['role'];
           //get the table name
           $tableName=$this->getTableName($role);
           //print $tableName;
           //Dont generate query with invalid table name
           if($role!="trainer")
           {
               $Query=sprintf("SELECT 1 from %s WHERE Name='%s' AND password='%s'",
               mysql_real_escape_string($tableName),
               mysql_real_escape_string($username),
               mysql_real_escape_string(md5($password))
               );
           }
           else
           {
               $Query=sprintf("SELECT 1 from %s WHERE Name='%s' AND password='%s' AND Approved=1",
               mysql_real_escape_string($tableName),
               mysql_real_escape_string($username),
               mysql_real_escape_string(md5($password))
               );
           }
         return $Query;
         }
        
        /*
        Postcondition:Returns true when has Required access and false otherwise
        */
        public function hasRequiredAccessLevel()
        {
           if(isset($_SESSION['access'])==false||$_SESSION['access']==-1)
           {
            return true;
           }
           else
           {
            
            return false;
           }
        }
        /*
        Postcondition:Returns true when has the necessary parameters
        */
        public function hasNecessaryParameters()
        {
            if(isset( $_POST['username'])==false||isset( $_POST['password'])==false||isset( $_POST['role'])==false||empty( $_POST['username'])==true||empty( $_POST['password'])==true||empty( $_POST['role'])==true)
            {
                
                
                return false;
            }
            else if( $_POST['role']!="trainee"&& $_POST['role']!="trainer"&&$_POST['role']!="admin")
            {
                 
                return false;
            }
           
            return true;
        }
         /*
        Postcondition:Generate a query to get the id of the person and returns it.
        */
        public function createGetIdRequest()
        {
            $username= $_POST['username'];
            $password= $_POST['password'];
            $tableName=$this->getTableName( $_POST['role']);
            $Query=sprintf("SELECT ID from %s WHERE Name='%s' AND password='%s'",
           mysql_real_escape_string($tableName),
           mysql_real_escape_string($username),
           mysql_real_escape_string(md5($password))
           );
           return $Query;
        }
    }
?>