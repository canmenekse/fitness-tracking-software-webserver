 <?php
 $root=$_SERVER['DOCUMENT_ROOT'];
 require_once($root . '/Webserver/Requesters/Requester.php');  
 
  class AssignTrainingProgramToTraineeRequester extends Requester
  {
  
      public function createRequest()
        {
            $trainee_ID=$_SESSION['trainee_ID'];
            $trainer_ID=$_SESSION['ID'];
            $training_program_ID=$_SESSION['training_Program_ID'];
            
            $query=sprintf("INSERT INTO trains (Training_Program_ID,Trainer_ID,Trainee_ID) values(%d,%d,%d)",mysql_real_escape_string($training_program_ID),mysql_real_escape_string($trainer_ID),
            mysql_real_escape_string($trainee_ID));
            return $query;
        }
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==1)
            {
                return true;
            }
            return false;
        }
        
        public function hasNecessaryParameters()
        {
            
            if(validVariable($_SESSION['trainee_ID'])==false||validVariable($_SESSION['ID'])==false||validVariable($_SESSION['training_Program_ID'])==false)
            {
                return false;
            }
            
            
            return true;
        }

       
         function __construct($databaseHandler)
        {
            
            parent:: __construct($databaseHandler);
            
        }
        public function getResult()
        {
            $this->databaseHandler->getQueryResult();
        }
  
 }
 
 
 
 
 
 
 
 
 
 
 ?>