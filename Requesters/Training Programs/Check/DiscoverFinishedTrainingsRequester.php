<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class DiscoverFinishedTrainingsRequester extends Requester
    {
        public function createRequest()
        {
            $query=sprintf("SELECT Training_Program_ID
FROM training_program 
WHERE Finalized = 1 AND Training_Program_ID IN
(
SELECT Training_Program_ID
FROM trains
WHERE Trainee_ID = %d AND Completed = 0 AND Finalized = 1 AND Training_Program_ID NOT IN

(
	SELECT tra.Training_Program_ID
	FROM training_program_content tra
	WHERE NOT EXISTS(
	SELECT t.Training_Program_ID, t.Exercise_ID
	FROM task_status t, training_program_content t2
	WHERE  t.Trainee_ID = %d AND t.Training_Program_ID=t2.Training_Program_ID AND t.Exercise_ID = t2.Exercise_ID AND t.Exercise_Count >= t2.Exercise_Count AND tra.Training_Program_ID = t.Training_Program_ID AND tra.Exercise_ID = t.Exercise_ID
)))",$_SESSION['ID'],$_SESSION['ID']);
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==0)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['ID'])==false||empty($_SESSION['ID'])==true)
            {
                return false;
            }
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>

