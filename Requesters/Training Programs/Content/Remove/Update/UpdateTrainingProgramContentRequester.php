<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  

    class UpdateTrainingProgramContentRequester extends Requester
    {
        public function createRequest()
        {
            //To decide whether update or Insert.
            
            $exercise_ID=$_POST['exercise_ID'];
            $count=$_POST['count'];
            
            
            $query=sprintf("UPDATE training_program_content SET Exercise_Count=%d WHERE Training_Program_ID =%d AND Exercise_ID=%d
            ",mysql_real_escape_string($count),mysql_real_escape_string($_POST['training_Program_ID']),
            mysql_real_escape_string($exercise_ID));
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if($_SESSION['access']==1)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
          if(validVariable($_SESSION['access'])==false||validVariable($_POST['training_Program_ID'])==false||validVariable($_POST['count'])==false
          ||validVariable($_POST['exercise_ID'])==false)
          {
                return false;
          }
           return true;
        }
    }
?>