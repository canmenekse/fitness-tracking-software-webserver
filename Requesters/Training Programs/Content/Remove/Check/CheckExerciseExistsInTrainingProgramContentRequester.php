<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    class CheckExerciseExistsInTrainingProgramContentRequester extends Requester
    {
        public function createRequest()
        {
            //To decide whether update or Insert.
            $trainingprogramID=$_SESSION['training_Program_ID'];
            $exerciseID=$_POST['exercise_ID'];
            $query=sprintf("SELECT 1 FROM training_program_content WHERE Training_Program_ID='%s'AND Exercise_ID=%d",
            mysql_real_escape_string($trainingprogramID),mysql_real_escape_string($exerciseID));
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])&&$_SESSION['access']>0)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
            
            if(validVariable($_SESSION['access'])==false||validVariable($_SESSION['training_Program_ID'])==false
            ||validVariable($_POST['exercise_ID'])==false)
            {
            return false;
            }
            return true;
        }
    }
?>