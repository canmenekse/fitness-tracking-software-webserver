<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  

    class InsertTrainingProgramContentRequester extends Requester
    {
        public function createRequest()
        {
            //To decide whether update or Insert.
            
            $exercise_ID=$_POST['exercise_ID'];
            $count=$_POST['count'];
            $training_Program_ID=$_POST['training_Program_ID'];
            
           
            $query=sprintf("INSERT INTO training_program_content (Training_Program_ID,Exercise_ID,Exercise_Count)
            values(%d,%d,%d)",
            mysql_real_escape_string($training_Program_ID),mysql_real_escape_string($exercise_ID),
            mysql_real_escape_string($count));
            
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if($_SESSION['access']==1)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
          if(validVariable($_SESSION['access'])==false||validVariable($_POST['training_Program_ID'])==false||validVariable($_POST['count'])==false
          ||validVariable($_POST['exerciseID'])==false)
          {
                return false;
          }
           return true;
        }
    }
?>