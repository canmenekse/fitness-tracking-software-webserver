<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class UpdateTrainingToCompletedRequester extends Requester
    {
        public function createRequest()
        {
            $query=sprintf("UPDATE trains SET Completed=1 WHERE Trainee_ID=%d AND Training_Program_ID=%d"
            ,mysql_real_escape_string($_SESSION['ID']),mysql_real_escape_string($_SESSION['training_Program_ID']));
            return $query;
        }
         
	
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==0)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['ID'])==false||empty($_SESSION['ID'])==true||
               isset($_SESSION['training_Program_ID'])==false||empty($_SESSION['training_Program_ID'])==true
            )
            {
                return false;
            }
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>

