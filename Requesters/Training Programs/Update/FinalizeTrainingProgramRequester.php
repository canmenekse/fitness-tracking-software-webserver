<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class FinalizeTrainingProgramRequester extends Requester
    {
        public function createRequest()
        {
            $query=sprintf("UPDATE training_program SET Finalized=1 WHERE Training_Program_ID=%d",mysql_real_escape_string($_POST['training_Program_ID']));
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==1)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(validVariable($_POST['training_Program_ID'])==false)
            {
                return false;
            }
            
           return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>