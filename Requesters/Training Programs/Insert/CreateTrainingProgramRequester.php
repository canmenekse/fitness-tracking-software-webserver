<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class CreateTrainingProgramRequester extends Requester
    {
        public function createRequest()
        {
            $name=$_POST['name'];
            $query=sprintf("INSERT INTO training_program (Training_Program_Name) values('%s')",mysql_real_escape_string($name));
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==1)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(validVariable($_POST['name'])==false)
            {
                return false;
            }
            
            
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>