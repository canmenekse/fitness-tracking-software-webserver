<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    class CheckExerciseExistsInActivityContentRequester extends Requester
    {
        public function createRequest()
        {
            //To decide whether update or Insert.
            $achievementID=$_SESSION['achievement_ID'];
            $exerciseID=$_GET['exerciseID'];
            $query=sprintf("SELECT 1 FROM achievement_content WHERE Achievement_ID='%s'AND Exercise_ID=%d",
            mysql_real_escape_string($achievementID),mysql_real_escape_string($exerciseID));
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])&&$_SESSION['access']==2)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
            
            if(validVariable($_SESSION['access'])==false||validVariable($_SESSION['achievement_ID'])==false
            ||validVariable($_GET['exerciseID'])==false)
            {
            return false;
            }
            return true;
        }
    }
?>