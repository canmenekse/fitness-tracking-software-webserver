<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  

    class InsertAchievementContentRequester extends Requester
    {
        public function createRequest()
        {
            //To decide whether update or Insert.
            
            $exercise_ID=$_GET['exerciseID'];
            $count=$_GET['count'];
            
            
            $query=sprintf("INSERT INTO achievement_content (Achievement_ID,Exercise_ID,Exercise_Count)
            values(%d,%d,%d)",
            mysql_real_escape_string($_SESSION['achievement_ID']),mysql_real_escape_string($exercise_ID),
            mysql_real_escape_string($count));
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if($_SESSION['access']==2)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
          if(validVariable($_SESSION['access'])==false||validVariable($_GET['exercise_ID'])==false||validVariable($_GET['count'])==false
          ||validVariable($_SESSION['achievement_ID'])==false)
          {
                return false;
          }
           return true;
        }
    }
?>