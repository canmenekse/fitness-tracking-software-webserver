<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    //CheckID Requester/
    class UpdateBonusRequester extends Requester
    {
        
        //Methods
       
        function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
      
        public function createRequest()
        {
         $traineeID=$_SESSION['ID'];
         $bonus=$_SESSION['Bonus'];
         
         $query=sprintf("UPDATE trainees SET Trainee_Bonus=Trainee_Bonus+%d WHERE ID=%d",
            mysql_real_escape_string($bonus),mysql_real_escape_string($traineeID));
            return $query;
         }
        
        /*
        Postcondition:Returns true when has Required access and false otherwise
        */
        public function hasRequiredAccessLevel()
        {
           if(isset($_SESSION['access'])==false||$_SESSION['access']==0)
           {
            return true;
           }
           else
           {
            return false;
           }
        }
        /*
        Postcondition:Returns true when has the necessary parameters
        */
        public function hasNecessaryParameters()
        {
            //Check whether the necessary parameters are set and non empty
            if(isset($_SESSION['Bonus'])==false||empty($_SESSION['Bonus'])==true||isset($_SESSION['access'])==false||empty($_SESSION['access'])==true)
            {
               
                
                return false;
            }
            
            return true;
        }
        
       
    }
?>