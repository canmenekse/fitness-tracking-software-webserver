<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  

    class UpdateAchievementContentRequester extends Requester
    {
        public function createRequest()
        {
            //To decide whether update or Insert.
            
            $exercise_ID=$_GET['exerciseID'];
            $count=$_GET['count'];
            
            
            $query=sprintf("UPDATE achievement_content SET Exercise_Count=%d WHERE Achievement_ID=%d AND Exercise_ID=%d
            ",mysql_real_escape_string($count),mysql_real_escape_string($_SESSION['achievement_ID']),
            mysql_real_escape_string($exercise_ID));
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if($_SESSION['access']==2)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
          if(validVariable($_SESSION['access'])==false||validVariable($_GET['exercise_ID'])==false||validVariable($_GET['count'])==false
          ||validVariable($_SESSION['achievement_ID'])==false)
          {
                return false;
          }
           return true;
        }
    }
?>