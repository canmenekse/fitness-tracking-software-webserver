<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class ViewAchievements extends Requester
    {
        public function createRequest()
        {
            $achievement_ID=$_SESSION['achievement_ID'];
            $query=sprintf("SELECT 1 FROM completed_achievements WHERE Trainee_ID=%d AND Achievement_ID=%d"
            ,$_SESSION['ID'],$_SESSION['achievement_ID'])
         
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if($_SESSION['access']==1)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['ID']==false||isset($_SESSION['achievement_ID']==false)||empty($_SESSION['ID']==true||
            $_SESSION['achievement_ID']==true)
            {
                return false;
            }
            return true;
        }
    }
?>