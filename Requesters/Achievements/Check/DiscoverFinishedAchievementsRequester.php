<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class DiscoverFinishedAchievementsRequester extends Requester
    {
        public function createRequest()
        {
            $query=sprintf(" 
       SELECT Achievement_ID FROM(
       SELECT Achievement_ID,Finalized
     
        FROM(
        SELECT Achievement_ID,Finalized
                FROM achievements
                WHERE Achievement_ID NOT IN
                (

                                    SELECT k.Achievement_ID
                                    FROM achievement_content k

                                    WHERE NOT EXISTS
                                    (
                                    SELECT a.Achievement_ID, a.Exercise_ID
                                    FROM achievement_content a
                                    INNER JOIN	(
                                                SELECT t.Exercise_ID, SUM(t.Exercise_Count) total
                                                FROM task_status t
                                                WHERE t.Trainee_ID =%d
                                                GROUP BY t.Exercise_ID
                                                ) fin
                                    ON a.Exercise_ID=fin.Exercise_ID
                                    WHERE fin.total > a.Exercise_Count AND a.Achievement_ID=k.Achievement_ID AND k.Exercise_ID = a.Exercise_ID
                                    )))Q WHERE Achievement_ID NOT IN  (SELECT Achievement_ID FROM completed_achievements WHERE
                                    Trainee_ID=%d) )P WHERE Finalized=1",mysql_real_escape_string($_SESSION['ID']),mysql_real_escape_string($_SESSION['ID']));
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==0)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['ID'])==false||empty($_SESSION['ID'])==true)
            {
                return false;
            }
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }

    
    
    /*
    SELECT Achievement_ID FROM(
 SELECT Achievement_ID,Finalized
     
        FROM(
        SELECT Achievement_ID,Finalized
                FROM achievements
                WHERE Achievement_ID NOT IN
                (

                                    SELECT k.Achievement_ID
                                    FROM achievement_content k

                                    WHERE NOT EXISTS
                                    (
                                    SELECT a.Achievement_ID, a.Exercise_ID
                                    FROM achievement_content a
                                    INNER JOIN	(
                                                SELECT t.Exercise_ID, SUM(t.Exercise_Count) total
                                                FROM task_status t
                                                WHERE t.Trainee_ID =%d
                                                GROUP BY t.Exercise_ID
                                                ) fin
                                    ON a.Exercise_ID=fin.Exercise_ID
                                    WHERE fin.total > a.Exercise_Count AND a.Achievement_ID=k.Achievement_ID AND k.Exercise_ID = a.Exercise_ID
                                    )))Q WHERE Achievement_ID NOT IN  (SELECT Achievement_ID FROM completed_achievements WHERE
                                    Trainee_ID=1) )P WHERE Finalized=1
    
    */
    
    
    
    
    
    ?>





