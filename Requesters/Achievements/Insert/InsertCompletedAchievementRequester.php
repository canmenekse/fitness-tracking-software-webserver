<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class InsertCompletedAchievementRequester extends Requester
    {
        public function createRequest()
        {
            $query=sprintf("INSERT INTO completed_achievements (Trainee_ID,Achievement_ID) values(%d,%d)"
            ,mysql_real_escape_string($_SESSION['ID']),mysql_real_escape_string($_SESSION['achievement_ID']));
            return $query;
        }
         
	
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==0)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['ID'])==false||empty($_SESSION['ID'])==true||
               isset($_SESSION['achievement_ID'])==false||empty($_SESSION['achievement_ID'])==true
            )
            {
                return false;
            }
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>

