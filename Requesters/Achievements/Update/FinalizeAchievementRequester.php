<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class FinalizeAchievementRequester extends Requester
    {
        public function createRequest()
        {
            
            
            $query=sprintf("UPDATE achievements SET Finalized=1 WHERE Achievement_ID=%d",mysql_real_escape_string($_SESSION['achievement_ID']));
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==2)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(validVariable($_SESSION['achievement_ID'])==false)
            {
                return false;
            }
            
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>