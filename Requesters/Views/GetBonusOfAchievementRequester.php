<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    //CheckID Requester/
    class GetBonusOfAchievementRequester extends Requester
    {
        
        //Methods
       
        function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
      
        public function createRequest()
        {
         $achievement_ID=$_SESSION['achievement_ID'];
         $query=sprintf("SELECT Achievement_Bonus FROM achievements WHERE Achievement_ID=%d",$achievement_ID);
         return $query;
         }
        
        /*
        Postcondition:Returns true when has Required access and false otherwise
        */
        public function hasRequiredAccessLevel()
        {
           if(isset($_SESSION['access'])==false||$_SESSION['access']==0)
           {
            return true;
           }
           else
           {
            return false;
           }
        }
        /*
        Postcondition:Returns true when has the necessary parameters
        */
        public function hasNecessaryParameters()
        {
            //Check whether the necessary parameters are set and non empty
            if(validVariable($_SESSION['achievement_ID'])==false)
            {
              
                
                return false;
            }
            
            return true;
        }
        
       
    }
?>