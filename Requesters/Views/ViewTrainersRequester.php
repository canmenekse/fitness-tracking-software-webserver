 <?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
     //ViewAssignedTrainers
     class ViewTrainersRequester extends Requester
     {
        //Gets all the necessary fields about trainers
        public function createRequest()
        {
            $Query=("SELECT ID,Name,Trainer_Email,Trainer_Phone,Approved from trainers WHERE Approved!=-1");
            return $Query;
        }
        //Admin access is required
        public function hasRequiredAccessLevel()
        {
            if($_SESSION['access']==2)
            {
                return true;
            }
            return false;
        }
        //Does not need extra parameters but we have to check $_SESSION['access']
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['access'])==false)
            {
                return false;
            }
            return true;
        }
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
     }
?>