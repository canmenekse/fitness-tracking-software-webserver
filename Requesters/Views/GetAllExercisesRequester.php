<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    class GetAllExercisesRequester extends Requester
    {
        public function createRequest()
        {
            //To decide whether update or Insert.
            
            $query=sprintf("SELECT Exercise_ID, Exercise_Name FROM exercises");
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])&&$_SESSION['access']>0)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
            
            if(validVariable($_SESSION['ID'])==false)
            {
            return false;
            }
            return true;
        }
    }
?>