<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class ViewAllAchievementsAdminRequester extends Requester
    {
        public function createRequest()
        {
            $query=sprintf("SELECT * FROM achievements WHERE Finalized=0");
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==2)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['ID'])==false||empty($_SESSION['ID'])==true)
            {
                return false;
            }
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>