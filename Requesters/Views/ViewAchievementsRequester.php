<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class ViewAchievementsRequester extends Requester
    {
        public function createRequest()
        {
            $query=sprintf("SELECT Achievement_Name,Achievement_ID FROM achievements WHERE Achievement_ID IN(
            SELECT Achievement_ID FROM completed_achievements WHERE Trainee_ID=%d) AND Finalized=1",$_SESSION['ID']);
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==0)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['ID'])==false||empty($_SESSION['ID'])==true)
            {
                return false;
            }
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>