<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class ViewAssignableTrainingProgramsRequester extends Requester
    {
        public function createRequest()
        {
            $query=sprintf("SELECT Training_Program_Name, Training_Program_ID FROM training_program WHERE Finalized=1");
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==1)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['ID'])==false||empty($_SESSION['ID'])==true)
            {
                return false;
            }
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
        public function getQueryResult()
        {
            return $this->databaseHandler->getQueryResult();
        }
    }
?>