<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class ViewFinishedTrainingProgramsAndExercisesStatusRequester extends Requester
    {
        public function createRequest()
        {
            $query=sprintf("Select t.Training_Program_ID ,exercisesTable.Unit,program.Training_Program_Name,exercisesTable.Exercise_Name,e.Exercise_ID,e.Exercise_Count,e2.Exercise_Count AS Done from training_program program ,trains t, training_program_content e, task_status e2,exercises exercisesTable where t.Completed = 1 And t.Training_Program_ID=e.Training_Program_ID And t.Training_Program_ID=e2.Training_Program_ID And e.Exercise_ID=e2.Exercise_ID AND t.Trainee_ID=%d AND program.Training_Program_ID=t.Training_Program_ID AND e.Exercise_ID=exercisesTable.Exercise_ID AND e2.Trainee_ID=%d",
mysql_real_escape_string($_SESSION['ID']),mysql_real_escape_string($_SESSION['ID']));
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==0)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['ID'])==false||empty($_SESSION['ID'])==true)
            {
                return false;
            }
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>

