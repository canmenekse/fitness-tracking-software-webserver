<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class ViewFinishedTrainingsRequester extends Requester
    {
        public function createRequest()
        {
            $query=sprintf("SELECT Training_Program_Name,Training_Program_ID FROM training_program WHERE Training_Program_ID IN(
            SELECT Training_Program_ID FROM trains WHERE Trainee_ID=%d AND Completed=1 AND Finalized=1)",$_SESSION['ID']);
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==0)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['ID'])==false||empty($_SESSION['ID'])==true)
            {
                return false;
            }
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>