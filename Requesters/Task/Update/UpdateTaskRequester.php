<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    class UpdateTaskRequester extends Requester
    {
        public function createRequest()
        {
            //To decide whether update or Insert.
            $ID=$_SESSION['ID'];
            $trainingProgramID=$_POST['trainingProgramID'];
            $exerciseID=$_POST['exerciseID'];
            $amount=$_POST['amount'];
            
            $query=sprintf("UPDATE task_status SET Exercise_Count=%d WHERE Trainee_ID=%d AND Training_Program_ID=%d AND Exercise_ID=%d",
            mysql_real_escape_string($amount),mysql_real_escape_string($ID),mysql_real_escape_string($trainingProgramID),
            mysql_real_escape_string($exerciseID));
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if($_SESSION['access']==0)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
          if(isset($_SESSION['ID'])==false||isset($_POST['trainingProgramID'])==false||isset($_POST['exerciseID'])==false||
          isset($_POST['amount'])==false||empty($_POST['trainingProgramID'])==true||empty($_POST['exerciseID'])==true||
          empty($_POST['amount'])==true)
            {
                return false;
            }
            return true;
        }
    }
?>