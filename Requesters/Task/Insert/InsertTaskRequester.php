<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    class InsertTaskRequester extends Requester
    {
        public function createRequest()
        {
            //To decide whether update or Insert.
            $ID=$_SESSION['ID'];
            $trainingProgramID=$_POST['trainingProgramID'];
            $exerciseID=$_POST['exerciseID'];
            $amount=$_POST['amount'];
            
            $query=sprintf("INSERT INTO task_status  (Training_Program_ID,Trainee_ID,Exercise_ID,Exercise_Count)
            values(%d,%d,%d,%d)"
            ,mysql_real_escape_string($trainingProgramID),mysql_real_escape_string($ID),
            mysql_real_escape_string($exerciseID),mysql_real_escape_string($amount));
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if($_SESSION['access']==0)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
          if(isset($_SESSION['ID'])==false||isset($_POST['trainingProgramID'])==false||isset($_POST['exerciseID'])==false||
          isset($_POST['amount'])==false||empty($_POST['trainingProgramID'])==true||empty($_POST['exerciseID'])==true||
          empty($_POST['amount'])==true)
          
            {
                return false;
            }
            return true;
        }
    }
?>