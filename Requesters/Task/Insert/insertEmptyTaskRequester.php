<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    class InsertEmptyTaskRequester extends Requester
    {
        public function createRequest()
        {
            //To decide whether update or Insert.
            $ID=$_SESSION['trainee_ID'];
            $trainingProgramID=$_SESSION['training_Program_ID'];
            $exerciseID=$_SESSION['exercise_ID'];
            $amount=$_SESSION['amount'];
            
            $query=sprintf("INSERT INTO task_status  (Training_Program_ID,Trainee_ID,Exercise_ID,Exercise_Count)
            values(%d,%d,%d,%d)"
            ,mysql_real_escape_string($trainingProgramID),mysql_real_escape_string($ID),
            mysql_real_escape_string($exerciseID),mysql_real_escape_string($amount));
            return $query;
         }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if($_SESSION['access']==0)
            {
                return true;
            }
            return false;
        }
        
        
        
         function __construct($databaseHandler)
        {
          parent:: __construct($databaseHandler);
            
        }
        public function hasNecessaryParameters()
        {
          if(validVariable($_SESSION['ID'])==false||validVariable($_SESSION['training_ProgramID'])==false||validVariable($_SESSION['exercise_ID'])==false||
          validVariable($_POST['amount'])==false||validVariable($_SESSION['trainee_ID']))
          
            {
                return false;
            }
            return true;
        }
    }
?>