<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
  class CreateExerciseRequester extends Requester
    {
        public function createRequest()
        {
            $name=$_POST['name'];
            $unit=$_POST['unit'];
            $query=sprintf("INSERT INTO exercises (Exercise_Name,Unit) values('%s','%s')",mysql_real_escape_string($name),mysql_real_escape_string($unit));
            return $query;
        }
         
						
        
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==true && $_SESSION['access']==1)
            {
                return true;
            }
            return false;
        }
        public function hasNecessaryParameters()
        {
            if(validVariable($_POST['name'])==false||validVariable($_POST['unit'])==false)
            {
                return false;
            }
            
            
            return true;
        }
        
        
        
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
    }
?>