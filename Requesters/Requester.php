<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Utilities/utilities.php');   
abstract class Requester
{
    //Data members
    protected $databaseHandler;
    protected $query;
    protected $requiredAccessLevel;
    protected $result;
    //Abstract Methods
    abstract public function createRequest();
    abstract public function hasRequiredAccessLevel();
    //Non abstract methods.
    public function sendRequest($query)
    {
        $this->databaseHandler->Query($query);
    }
    function __construct($databaseHandler)
    {   
        $this->databaseHandler=$databaseHandler;
        
    }
    public function getDatabaseHandler()
    {
        return $this->databaseHandler;
    }
    public function getQueryResult()
    {
        return $this->databaseHandler->getQueryResult();
    }
 }



?>