<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    //CheckUser Requester
    class CheckUserExistRequester extends Requester
    {
        
        //Methods
       
        function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
        /*
        Postcondition:returns the table name
        */
        public function getTableName($role,&$email_placeholder)
        {
            if($role=="trainer"||$role=="trainee")
            {
              //'trainer' become 'trainers','trainee' become 'trainees'
              $tableName=$role."s";
              $email_placeholder=$role."_email";
            }
            else
            {
             $tableName="";
            }
           return $tableName;
        }
        /*
        Precondition:Has the necessary parameters
        Postcondition:Returns a query
        */
        public function createRequest()
        {
           //Get the parameters
           $username=$_POST['username'];
           $role=$_SESSION['role'];
           $email=$_POST['email'];
           $email_placeholder="";
           //get the table name
           $tableName=$this->getTableName($role,$email_placeholder);
          //Dont generate query with invalid table name
           $Query=sprintf("SELECT 1 from %s WHERE Name='%s' OR %s ='%s'",
           mysql_real_escape_string($tableName),
           mysql_real_escape_string($username),
           mysql_real_escape_string($email_placeholder),
           mysql_real_escape_string($email)
           );
         return $Query;
         }
        
        /*
        Postcondition:Returns true when has Required access and false otherwise
        */
        public function hasRequiredAccessLevel()
        {
           if(isset($_SESSION['access'])==false||$_SESSION['access']==-1)
           {
            return true;
           }
           else
           {
            return false;
           }
        }
        /*
        Postcondition:Returns true when has the necessary parameters
        */
        public function hasNecessaryParameters()
        {
            //Check whether the necessary parameters are set and non empty
            if(isset( $_POST['username'])==false||isset($_SESSION['role'])==false||isset( $_POST['email'])==false
            ||empty($_POST['username'])==true||empty($_SESSION['role'])==true||empty( $_POST['email'])==true)
            {
                
                
                return false;
            }
            //Checks whether session is checked
            else if($_SESSION['role']!="trainee"&&$_SESSION['role']!="trainer")
            {
                return false;
            }
            //Check whether user entered an valid email
            else if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)==false)
            {
                
                return false;
            }
            return true;
        }
         /*
        Postcondition:Generate a query to get the id of the person and returns it.
        */
       
    }
?>