<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  
    //CreateAccount Requester
    class CreateTrainerAccountRequester extends Requester
    {
        /*Precondition:Has the necessary parameters
        Postcondition:Returns a query*/
        public function createRequest()
        {
           //initialize
           $username= $_POST['username'];
           $email= $_POST['email'];
           $password= $_POST['password'];
		   $phone = $_POST['phone'];
           //create the query
           $Query=sprintf("INSERT INTO trainers (Name,Password,Trainer_Email,Trainer_Phone) values('%s','%s','%s','%s')",
           mysql_real_escape_string($username),
           mysql_real_escape_string(md5($password)),
           mysql_real_escape_string($email),
		   mysql_real_escape_string($phone)
           );
           return $Query;
        }
         /*
        Postcondition:Returns true when has Required access and false otherwise
        */
        public function hasRequiredAccessLevel()
        {
            if(isset($_SESSION['access'])==false||$_SESSION['access']==-1)
           {
            return true;
           }
           else
           {
            return false;
           }
        }
        //Constructor
        function __construct($databaseHandler)
        {
            parent:: __construct($databaseHandler);
        }
        /*
        Postcondition:Returns true when has the necessary parameters
        */
        public function hasNecessaryParameters()
        {
            //Check whether the necessary parameters are set and non empty
            if(validVariable($_POST['username'])==false||validVariable($_POST['password'])==false||validVariable(['email'])==false||validVariable(['phone'])==false)
            {
                    print "printed because of that ";
                    return false;
            
            }
            //Check E mail
            else if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)==false) /*preg_match("/[a-zA-Z]/", $_POST['phone']) === 0*/
            {
                 print "printed because of that 2 ";
                return false;
            }
            return true;
            
        }
    }
 ?>