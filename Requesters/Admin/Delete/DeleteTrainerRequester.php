<?php
   require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Requester.php');  

     class DeleteTrainerRequester extends Requester
    {
        public function createRequest()
        {
            //The id sent here (hence the id in $_SESSION['sentID']) must be validated.
             $ID=$_SESSION['sentID'];
            $query=sprintf("UPDATE trainers SET Approved=%d WHERE ID=%d",-1,mysql_real_escape_string($ID));
            return $query;
            
        }
        //Check whether it is the admin who requested deletion
        public function hasRequiredAccessLevel()
        {
            if($_SESSION['access']==2)
            {
                return true;
            }
            return false;
        }
        //Constructor
         function __construct($databaseHandler)
        {
            
            
            parent:: __construct($databaseHandler);
            
        }
        //Check the necessary parameters
        public function hasNecessaryParameters()
        {
            if(isset($_SESSION['access'])==false||isset($_GET['ID'])==false||empty($_GET['ID'])==true)
            {
                ;
                return false;
            }
            //We have to make sure that it is an integer.
            else
            {
                $ID=$_GET['ID'];
                if(is_numeric($ID)==true)
                {
                    return true;
                }
                else
                {
                    //print "heyawyheayuweh";
                    return false;
                }
                
            }
            return true;
        }
    }
?>