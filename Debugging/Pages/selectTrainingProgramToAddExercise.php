<?php
 
  require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/Requesters/Views/ViewAddableTrainingProgramsRequester.php'); 
  require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/DatabaseHandler.php'); 
    require_once($_SERVER['DOCUMENT_ROOT'] . '/Webserver/LoginManager.php'); 
   
  session_start();
  $databaseHandler=new DatabaseHandler();
   header('Content-Type: application/json');
  $messageArray=array();
  //Generate a Request to check the user
  $viewAddableTrainingsRequester=new ViewAddableTrainingProgramsRequester($databaseHandler);
  //Checks accessLevel
  if($viewAddableTrainingsRequester->hasRequiredAccessLevel()==true)
  {
    if($viewAddableTrainingsRequester->hasNecessaryParameters()==true)
    {
    //Create the request
    $query=$viewAddableTrainingsRequester->createRequest();
    #print $query;
    $viewAddableTrainingsRequester->sendRequest($query);
    $queryResult=$viewAddableTrainingsRequester->getDatabaseHandler()->getQueryResult();
    //Get the query and put them in array
    $i=0;
    while( $row=mysqli_fetch_assoc($queryResult))
        {
            $i++;
           
           JSONMessageAddtoArray($messageArray,$i,$row['Training_Program_Name']);
        }
    }
    
    else
    {
        JSONMessageAddtoArray($messageArray,"Error","You do not have the necessary parameters");
    }
  
  
  }
  
  else
  {
    JSONMessageAddToArray($messageArray,"Error","You do not have the necessary permissions");
  }
    JSONMessageOutputter($messageArray);





?>

 