<?php
class DatabaseHandler
{
    //Data members
    private $id;
    private $connection;
    private $databaseName;
    private $host;
    private $password;
    private $port;
    private $queryResult;
    private $user;
    //Methods
    
    //Constructor
    /*it gets the necessary parameters to connect to the database
      But beware that when you give the port number as a string like
      "3392" it does not work
      Precondition:There must be a valid Database
      Postcondition:A databaseHandler that could be used to connect
      to database is created.
    */
    public  function __construct()
    {
        
        //Assigning the values
        $this->id=1;
        $this->databaseName="trainingdatabase";
        $this->user="root";
        $this->host="localhost";
        $this->password="";
        $this->port=3392;
        //Creating the mysqli connection that is used to communicate with database
        $this->connection=new mysqli($this->host,$this->user,$this->password,$this->databaseName,$this->port);
        //Check Whether the connection was succesful.
            if (mysqli_connect_error()) {
            die('Connect Error (' . mysqli_connect_errno() . ') '
                . mysqli_connect_error());
            }
            else
            {
           // print "connected".$id;
        }
        

   }
   //Returns the id of the Handler useful when debugging
   public function getHandlerId()
   {
    return $this->id;
   }
   public function Query($Query)
   {
           $this->queryResult= $this->connection->query($Query) or die(mysqli_error($this->connection));
           
   }
   public function getConnection()
   {
        return $this->connection;
   }
   public function getQueryResult()
   {
       return $this->queryResult;
   }
   
}
?>