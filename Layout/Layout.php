<?php
function printHeaders($title)
{
print('
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>'.$title.'</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--Style Files-->
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="assets/css/docs.css" rel="stylesheet">
        <link href="assets/js/google-code-prettify/prettify.css" rel="stylesheet">
        <link href="assets/css/styles.css" rel="stylesheet"/>
        
        
     

   
    </head>');
}

function printNavigationbar()
{
print('<div class="navbar navbar-inverse navbar-fixed-top">
           <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li >
                                <a href="trainerOperations.php">Trainers</a>
                                
                            </li>
                            <li>
                                <a href="../Pages/Logout/Logout.php">Logout</a>
                            </li>
                            <li>
                                <a href="addAchievements.php">Add Achievement</a>
                            </li>
                            <li>
                                <a href="addExercisesToAchievements.php" >Add Exercises to achievements</a>
                            </li>
                            
                        </ul>
                    </div>   
                 
                </div>
           </div>
        </div>');
}

function printScrpt()
{
    print('<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <script src="assets/js/bootstrap-affix.js"></script>

    <script src="assets/js/holder/holder.js"></script>
    <script src="assets/js/google-code-prettify/prettify.js"></script>

    <script src="assets/js/application.js"></script>');
}
function printErrorMessage($title,$message)
{
    $display=sprintf('<div class="container">
    <div class="alert alert-error">  
         
        <strong>%s</strong>%s</div></div>',$title,$message);
        print $display;
    
    
}
function printSucceedMessage($title,$message)
{
    $display=sprintf('<div class="container">
    <div class="alert alert-success">  
         
        <strong>%s</strong>%s</div></div>',$title,$message);
        print $display;
    
    
}
function printAdminPanelHeaders()
{
     print('<div class="container" id ="tble">
            <table class="table table-striped table-bordered table-condensed">
            <th>ID</th><th>Name</th><th>Email</th><th>Phone</th><th>Status</th>
            ');
}
function printExerciseHeaders()
{

     print('<div class="container" id ="tble">
            <table class="table table-striped table-bordered table-condensed">
            <th>ID</th><th>Achievement Name</th><th>Bonus</th><th>Type</th><th>Add</th><th>Finalize</th>
            ');
}

function printLoginMenu()
{
    print('<div class="container" style="margin-top:72px;">

      <form class="form-signin" action="http://localhost/Webserver/Pages/Login/login.php" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" name="username" class="input-block-level" placeholder="Username" id ="username">
        <input type="password" name="password" class="input-block-level" placeholder="Password">
        <input type="hidden" name="role" value="admin">
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
      </form>

    </div>');
}

function printcreateNewAchievement()
{
    print('<div class="container" style="margin-top:73px;">

      <form method="get" action="confirmAchievementAdd.php">
  <fieldset>
    <legend>Create new Achievement</legend>
    <label><strong>Name</strong></label>
    <input type="text" name="name" id="nameEditBox" placeholder="Enter achievement name">
    <label><strong>Type</strong></label>
    <input type="text" name="type" placeholder="Enter achievement type">
    
    <label><strong>Bonus</strong> </label>
    <input type="text" name="bonus" placeholder="Enter Bonus">
    <label></label>
    <button type="submit" class="btn">Create</button>
  </fieldset>
</form>');


}
function printSelectionForm()
{
 print('<div class="container" style="margin-top:73px;">

      <form method="get" action="confirmAchievementAdd.php">
  <fieldset>
  <label><strong>Exercises:</strong> </label>
  <select>
  <option value="volvo">Volvo</option>
  <option value="saab">Saab</option>
  <option value="opel">Opel</option>
  <option value="audi">Audi</option>
</select>
    
    <label><strong>Count</strong></label>
    
    <input type="text" name="count" id="nameEditBox" placeholder="Enter amount">
   <label></label>
    <button type="submit" class="btn">Add</button>
  </fieldset>
</form>');

}



















?>