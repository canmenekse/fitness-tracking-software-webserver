-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2013 at 06:13 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trainingdatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `achievements`
--

CREATE TABLE IF NOT EXISTS `achievements` (
  `Achievement_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Achievement_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Achievement_Bonus` int(11) NOT NULL,
  `Achievement_Type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Finalized` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Achievement_ID`),
  UNIQUE KEY `Achievement_Name` (`Achievement_Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `achievements`
--

INSERT INTO `achievements` (`Achievement_ID`, `Achievement_Name`, `Achievement_Bonus`, `Achievement_Type`, `Finalized`) VALUES
(1, 'Athlete', 19, 'Fitness', 1),
(2, 'Champion', 22, 'Fitness', 1);

-- --------------------------------------------------------

--
-- Table structure for table `achievement_content`
--

CREATE TABLE IF NOT EXISTS `achievement_content` (
  `Achievement_ID` int(11) NOT NULL,
  `Exercise_ID` int(11) NOT NULL,
  `Exercise_Count` int(11) NOT NULL,
  PRIMARY KEY (`Achievement_ID`,`Exercise_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `achievement_content`
--

INSERT INTO `achievement_content` (`Achievement_ID`, `Exercise_ID`, `Exercise_Count`) VALUES
(1, 1, 10),
(2, 2, 370);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `ID` int(7) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name` (`Name`,`Email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`ID`, `Name`, `Password`, `Email`) VALUES
(1, 'admin', 'e9215b2b4ed9534c57fe703069a3a2a8', 'adminogullari@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `completed_achievements`
--

CREATE TABLE IF NOT EXISTS `completed_achievements` (
  `Trainee_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Achievement_ID` int(11) NOT NULL,
  PRIMARY KEY (`Trainee_ID`,`Achievement_ID`),
  KEY `Achievement_ID` (`Achievement_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `completed_achievements`
--

INSERT INTO `completed_achievements` (`Trainee_ID`, `Achievement_ID`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exercises`
--

CREATE TABLE IF NOT EXISTS `exercises` (
  `Exercise_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Exercise_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Exercise_ID`),
  UNIQUE KEY `Exercice_Name` (`Exercise_Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

--
-- Dumping data for table `exercises`
--

INSERT INTO `exercises` (`Exercise_ID`, `Exercise_Name`) VALUES
(31, 'asdcc'),
(28, 'banabakit'),
(26, 'bhbjh'),
(27, 'bhbjha'),
(35, 'bilbakaim'),
(25, 'fdsre'),
(30, 'ffrfr'),
(34, 'Hello'),
(36, 'Hello2'),
(24, 'Hiking'),
(2, 'jump'),
(33, 'momnke'),
(29, 'pushup'),
(1, 'run'),
(32, 'vvv');

-- --------------------------------------------------------

--
-- Table structure for table `task_status`
--

CREATE TABLE IF NOT EXISTS `task_status` (
  `Training_Program_ID` int(11) NOT NULL,
  `Trainee_ID` int(11) NOT NULL,
  `Exercise_ID` int(11) NOT NULL,
  `Exercise_Count` int(11) NOT NULL,
  PRIMARY KEY (`Training_Program_ID`,`Trainee_ID`,`Exercise_ID`),
  KEY `Trainee_ID` (`Trainee_ID`),
  KEY `Exercise_ID` (`Exercise_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_status`
--

INSERT INTO `task_status` (`Training_Program_ID`, `Trainee_ID`, `Exercise_ID`, `Exercise_Count`) VALUES
(1, 1, 1, 9),
(1, 1, 2, 288),
(2, 1, 2, 271),
(42, 1, 26, 0),
(42, 1, 27, 0),
(43, 1, 1, 0),
(43, 1, 2, 0),
(43, 1, 29, 0);

-- --------------------------------------------------------

--
-- Table structure for table `trainees`
--

CREATE TABLE IF NOT EXISTS `trainees` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Trainee_Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Trainee_Bonus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Trainee_Name` (`Name`),
  UNIQUE KEY `Trainee_Email` (`Trainee_Email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `trainees`
--

INSERT INTO `trainees` (`ID`, `Name`, `Password`, `Trainee_Email`, `Trainee_Bonus`) VALUES
(1, 'a', '8cdee5526476b101869401a37c03e379', 'TempOglu@gmail.com', 22),
(3, 'Gahah', 'ebd5057f5e0c63cd89fb41e30f3c0077', 'ays@hay.com', 0),
(4, 'Bok', '3bec65f6f8000fd631db1300fd420986', 'hsh@hah.com', 0),
(5, 'Gshsusiu', '22928a70ecefb976902c56c2b5c10289', 'hdui@hsu.com', 0),
(6, 'z', 'fbade9e36a3f36d3d676c1b808451dd7', 'z@z.com', 0),
(7, 'Ahmet', 'cf00c3dfdb61eb1343846e95a9789315', 'aah@hui.com', 0),
(8, 'Capulcu', 'b886fbf2519eaf934437710b34b9f270', 'capul@cu.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE IF NOT EXISTS `trainers` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Trainer_Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Trainer_Phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Approved` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `trainers`
--

INSERT INTO `trainers` (`ID`, `Name`, `Password`, `Trainer_Email`, `Trainer_Phone`, `Approved`) VALUES
(1, 'Irkovic', 'b4b3e8c03529b83182fbddbee3ec6858', 'irkovic@gmail.com', '07587733534', 1),
(7, 'Hello', '4229d691b07b13341da53f17ab9f2416', 'gh@di.com', '555', 0),
(8, 'Can', '2c61ebff5a7f675451467527df66788d', 'can@can.com', '555', 1),
(9, 'Ahmet', 'cdb5efc9c72196c1bd8b7a594b46b44f', 'ahmet@gmail.com', '8887', 0),
(10, 'Belesco', '7815696ecbf1c96e6894b779456d330e', 'beles@as.com', '255', 0);

-- --------------------------------------------------------

--
-- Table structure for table `training_program`
--

CREATE TABLE IF NOT EXISTS `training_program` (
  `Training_Program_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Training_Program_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Finalized` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Training_Program_ID`),
  UNIQUE KEY `training_program_name` (`Training_Program_Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=44 ;

--
-- Dumping data for table `training_program`
--

INSERT INTO `training_program` (`Training_Program_ID`, `Training_Program_Name`, `Finalized`) VALUES
(1, 'Get fit training', 1),
(2, 'Have a good running tempo', 1),
(42, 'Bbanabakgit', 1),
(43, 'hard work', 1);

-- --------------------------------------------------------

--
-- Table structure for table `training_program_content`
--

CREATE TABLE IF NOT EXISTS `training_program_content` (
  `Training_Program_ID` int(11) NOT NULL,
  `Exercise_ID` int(11) NOT NULL,
  `Exercise_Count` int(11) NOT NULL,
  PRIMARY KEY (`Training_Program_ID`,`Exercise_ID`),
  KEY `Exercise_ID` (`Exercise_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `training_program_content`
--

INSERT INTO `training_program_content` (`Training_Program_ID`, `Exercise_ID`, `Exercise_Count`) VALUES
(1, 1, 12),
(1, 2, 20),
(2, 2, 10),
(42, 26, 234),
(42, 27, 11),
(43, 1, 500),
(43, 2, 50),
(43, 29, 30);

-- --------------------------------------------------------

--
-- Table structure for table `trains`
--

CREATE TABLE IF NOT EXISTS `trains` (
  `Trainer_ID` int(11) NOT NULL,
  `Trainee_ID` int(11) NOT NULL,
  `Training_Program_ID` int(11) NOT NULL,
  `Completed` int(2) NOT NULL,
  PRIMARY KEY (`Trainer_ID`,`Trainee_ID`,`Training_Program_ID`),
  KEY `Trainee_ID` (`Trainee_ID`),
  KEY `Training_Program_ID` (`Training_Program_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `trains`
--

INSERT INTO `trains` (`Trainer_ID`, `Trainee_ID`, `Training_Program_ID`, `Completed`) VALUES
(1, 1, 1, 0),
(1, 1, 2, 1),
(8, 1, 42, 0),
(8, 1, 43, 0),
(8, 4, 2, 0),
(8, 4, 43, 0),
(8, 6, 1, 0),
(8, 7, 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
