<?php 
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver//DatabaseHandler.php');
    require_once($root.'/Webserver//Utilities/utilities.php');
    require_once($root.'/Webserver/Requesters/Views/ViewAllAchievementsAdminRequester.php');
    session_start();
      $databaseHandler=new DatabaseHandler();
      $viewAllAchievementsRequester= new ViewAllAchievementsAdminRequester($databaseHandler);
      printHeaders("Select An achievement");
      print('<body>');
      print('<div class="container">');
     
      if($viewAllAchievementsRequester->hasNecessaryParameters()==true&&$viewAllAchievementsRequester->hasRequiredAccessLevel()==true)
      {
      //Layout related
             print('<div class="page-header,margin_to_middle">
            <h4>Add Exercises to Achievements <small>//Panel to add exercises to achievements</small></h4>
            </div></div>');
      
      
            $query=$viewAllAchievementsRequester->createRequest();
            //print "heyehehyeyh".$query;
            $viewAllAchievementsRequester->sendRequest($query);
            $queryResult=$viewAllAchievementsRequester->getQueryResult();
            printExerciseHeaders();
            while($row=$queryResult->fetch_assoc())
            {
                
                $singleRowPrint=createAchievementRow($row);
                print($singleRowPrint);
                
                
            }
            print('</table></div>');
      }
      else
      {
        printErrorMessage("Warning!","You are not logged in");
        waitAndRedirect('Webserver/Admin Panel/adminLogin.php',2);
      }
      
      printNavigationbar();
      //
    
      printScrpt();
      print('</body>');
      print('</html>');
?>

     