<?php
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver//DatabaseHandler.php');
    require_once($root.'/Webserver//Utilities/utilities.php');
    require_once($root.'/Webserver/Requesters/Achievements/Content/Insert/InsertAchievementContentRequester.php');
    require_once($root.'/Webserver/Requesters/Achievements/Content/Check/CheckExerciseExistsInAchievementContentRequester.php');
    require_once($root.'/Webserver/Requesters/Achievements/Content/Update/UpdateAchievementContentRequester.php');
    session_start();
    printHeaders("Approve");
    $databaseHandler=new DatabaseHandler();
    $checkExerciseExistsInActivityContentRequester=new CheckExerciseExistsInActivityContentRequester($databaseHandler);
    
    
    
    $messageArray = array();
    print('<body>');
    if($checkExerciseExistsInActivityContentRequester->hasRequiredAccessLevel())
    {
        
        if($checkExerciseExistsInActivityContentRequester->hasNecessaryParameters())
        {
            
            
            
            $query=$checkExerciseExistsInActivityContentRequester->createRequest();
            
            
            
            $checkExerciseExistsInActivityContentRequester->sendRequest($query);
            $queryResult=$checkExerciseExistsInActivityContentRequester->getQueryResult();
             if(mysqli_num_rows($queryResult)!=1)
            {
               $insertAchievementContentRequester=new InsertAchievementContentRequester($databaseHandler);
               $query=$insertAchievementContentRequester->createRequest();
               $insertAchievementContentRequester->sendRequest($query);
                printSucceedMessage("Success"," Added new Achievement ");
               waitAndRedirect("Webserver/Admin Panel/addExercisesToAchievements.php",2);
             
    
            }
            else
            {
                $updateAchievementContentRequester=new UpdateAchievementContentRequester($databaseHandler);
                
                $query=$updateAchievementContentRequester->createRequest();
                $updateAchievementContentRequester->sendRequest($query);
			 printSucceedMessage("Success"," Updated new Achievement ");
               waitAndRedirect("Webserver/Admin Panel/addExercisesToAchievements.php",2);
                
                    
                
                
            }
        }
        else
        {
			JSONMessageAddtoArray($messageArray,"Warning","You do not have the necessary parameters");
            waitAndRedirect("Webserver/Admin Panel/addAchievements.php",2);
        }
    
    
    }
    
    else
    {
		JSONMessageAddtoArray($messageArray,"Warning","You are not logged in");
        waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",2);
    }
    
     
      
      //
    JSONMessageOutputter($messageArray);          
      printScrpt();
      print('</body>');
      print('</html>');


?>  
  
  
  
  
  
  
  
