<?php
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/LoginManager.php');
    require_once($root.'/Webserver/Requesters/Login/Check/CheckIDExistRequester.php');
    require_once($root.'/Webserver/Requesters/Admin/Delete/DeleteTrainerRequester.php');
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver//DatabaseHandler.php');
    require_once($root.'/Webserver//Utilities/utilities.php');
    session_start();
    
    //Initializations
    $databaseHandler=new DatabaseHandler();
    $deleteTrainerRequester=new DeleteTrainerRequester($databaseHandler);
    
    //Print page
    printHeaders("Delete Trainer");
    
    print('<body>');
    if($deleteTrainerRequester->hasRequiredAccessLevel())
    {
        $_SESSION['role']="trainer";
        //It is OK to search with that id
        if($deleteTrainerRequester->hasNecessaryParameters())
        {
            
            $_SESSION['sentID']=$_GET['ID'];
            //Maybe ID is not in the database
            $checkIDExistRequester=new CheckIDExistRequester($databaseHandler);
            
            $query=$checkIDExistRequester->createRequest();
            
            
            $checkIDExistRequester->sendRequest($query);
            $queryResult=$checkIDExistRequester->getQueryResult();
             if(mysqli_num_rows($queryResult)!=1)
            {
                printErrorMessage("Warning!","You can not delete a non existing ID");
                waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",2);
            }
            else
            {
                $query=$deleteTrainerRequester->createRequest();
                $deleteTrainerRequester->sendRequest($query);
                $queryResult=$deleteTrainerRequester->getQueryResult();
                printSucceedMessage("Success","Removed from the database ");
                waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",2);
                
                
                
                
            }
        }
        else
        {
            printErrorMessage("Warning!","You do not have the necessary parameters");
            waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",2);
        }
    
    
    }
    
    else
    {
        printErrorMessage("Warning!","You are not logged in");
        waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",2);
    }
    
     
      //printNavigationbar();
      //
     
      printScrpt();
      print('</body>');
      print('</html>');


?>