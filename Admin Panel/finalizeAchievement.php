<?php
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver//DatabaseHandler.php');
    require_once($root.'/Webserver//Utilities/utilities.php');
    require_once($root.'/Webserver/Requesters/Achievements/Update/FinalizeAchievementRequester.php');
    session_start();
    printHeaders("Approve");
   $databaseHandler=new DatabaseHandler();
    $finalizeAchievementRequester=new FinalizeAchievementRequester($databaseHandler);
    
    
    print('<body>');
    if($finalizeAchievementRequester->hasRequiredAccessLevel())
    {
        
        if($finalizeAchievementRequester->hasNecessaryParameters())
        {
            
            
            $query=$finalizeAchievementRequester->createRequest();
            $finalizeAchievementRequester->sendRequest($query);
            printSucceedMessage("Succeed","Achievement is now Entered into database");
            waitAndRedirect("Webserver/Admin Panel/addExercisesToAchievements.php",2);
        }
        else
        {
            printErrorMessage("Warning!","You do not have the necessary parameters");
            waitAndRedirect("Webserver/Admin Panel/addAchievements.php",2);
        }
    
    
    }
    
    else
    {
        printErrorMessage("Warning!","You are not logged in");
        waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",2);
    }
    
     
      
      //
     
      printScrpt();
      print('</body>');
      print('</html>');


?>  
  
  
  
  
  
  
  
