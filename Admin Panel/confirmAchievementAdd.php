<?php
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver//DatabaseHandler.php');
    require_once($root.'/Webserver//Utilities/utilities.php');
    require_once($root.'/Webserver/Requesters/Achievements/Insert/CreateAchievementRequester.php');
    require_once($root.'/Webserver/Requesters/Achievements/Check/CheckAchievementExistsRequester.php');
    session_start();
    printHeaders("Approve");
    $databaseHandler=new DatabaseHandler();
    $createAchievementRequester=new CreateAchievementRequester($databaseHandler);
    
    
    
    
    print('<body>');
    if($createAchievementRequester->hasRequiredAccessLevel())
    {
        
        if($createAchievementRequester->hasNecessaryParameters())
        {
            
            
            $checkAchievementExistsRequester=new CheckAchievementExistsRequester($databaseHandler);
            $query=$checkAchievementExistsRequester->createRequest();
            
            
            $checkAchievementExistsRequester->sendRequest($query);
            $queryResult=$checkAchievementExistsRequester->getQueryResult();
             if(mysqli_num_rows($queryResult)!=1)
            {
                $createAchievementRequester= new createAchievementRequester($databaseHandler);
               $query=$createAchievementRequester->createRequest();
               $createAchievementRequester->sendRequest($query);
               $queryResult=$createAchievementRequester->getQueryResult();
               printSucceedMessage("Success"," Added new Achievement ");
               waitAndRedirect("Webserver/Admin Panel/addAchievements.php",2);
            }
            else
            {
               printErrorMessage("Error"," Duplicate Achievement ");
               waitAndRedirect("Webserver/Admin Panel/addAchievements.php",2);
                
                
                
                
                
                
                
            }
        }
        else
        {
            printErrorMessage("Warning!","You do not have the necessary parameters");
            waitAndRedirect("Webserver/Admin Panel/addAchievements.php",2);
        }
    
    
    }
    
    else
    {
        printErrorMessage("Warning!","You are not logged in");
        waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",2);
    }
    
     
      
      //
     
      printScrpt();
      print('</body>');
      print('</html>');


?>  
  
  
  
  
  
  
  
