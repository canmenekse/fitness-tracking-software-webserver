<?php
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/LoginManager.php');
    require_once($root.'/Webserver/Requesters/Login/Check/CheckIDExistRequester.php');
    require_once($root.'/Webserver/Requesters/Admin/Approve/ApproveTrainerRequester.php');
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver//DatabaseHandler.php');
    require_once($root.'/Webserver//Utilities/utilities.php');
    session_start();
    printHeaders("Approve");
    $databaseHandler=new DatabaseHandler();
    $approveTrainerRequester=new ApproveTrainerRequester($databaseHandler);
    
    
    print('<body>');
    if($approveTrainerRequester->hasRequiredAccessLevel())
    {
        $_SESSION['role']="trainer";
        if($approveTrainerRequester->hasNecessaryParameters())
        {
            
            $_SESSION['sentID']=$_GET['ID'];
            
            $checkIDExistRequester=new CheckIDExistRequester($databaseHandler);
            
            $query=$checkIDExistRequester->createRequest();
            
            
            $checkIDExistRequester->sendRequest($query);
            $queryResult=$checkIDExistRequester->getQueryResult();
             if(mysqli_num_rows($queryResult)!=1)
            {
                printErrorMessage("Warning!","You can not Approve a non existing ID");
                waitAndRedirect("trainerOperations.php",2);
            }
            else
            {
                $query=$approveTrainerRequester->createRequest();
                $approveTrainerRequester->sendRequest($query);
                $queryResult=$approveTrainerRequester->getQueryResult();
                printSucceedMessage("Success","Approved ");
                waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",2);
                
                
                
                
            }
        }
        else
        {
            printErrorMessage("Warning!","You do not have the necessary parameters");
            waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",2);
        }
    
    
    }
    
    else
    {
        printErrorMessage("Warning!","You are not logged in");
        waitAndRedirect("Webserver/Admin Panel/trainerOperations.php",2);
    }
    
     
      
      //
     
      printScrpt();
      print('</body>');
      print('</html>');


?>