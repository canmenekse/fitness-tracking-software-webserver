<?php 
    $root=$_SERVER['DOCUMENT_ROOT'];
    require_once($root.'/Webserver/Layout/Layout.php');
    require_once($root.'/Webserver/DatabaseHandler.php');
    require_once($root.'/Webserver/Utilities/utilities.php');
    require_once($root.'/Webserver/Requesters/Views/ViewTrainersRequester.php');
    session_start();
      $databaseHandler=new DatabaseHandler();
      $viewTrainersRequester= new ViewTrainersRequester($databaseHandler);
      printHeaders("View Trainers");
      print('<body>');
      print('<div class="container">');
     
      if($viewTrainersRequester->hasNecessaryParameters()==true&&$viewTrainersRequester->hasRequiredAccessLevel()==true)
      {
      //Layout related
             print('<div class="page-header,margin_to_middle">
            <h4>Trainers <small>//Panel to delete or approve trainers</small></h4>
            </div></div>');
      
      
            $query=$viewTrainersRequester->createRequest();
            //print "heyehehyeyh".$query;
            $viewTrainersRequester->sendRequest($query);
            $queryResult=$viewTrainersRequester->getQueryResult();
            printAdminPanelHeaders();
            while($row=$queryResult->fetch_assoc())
            {
                
                $singleRowPrint=createTrainerRow($row);
                print($singleRowPrint);
                
                
            }
            print('</table></div>');
      }
      else
      {
        printErrorMessage("Warning!","You are not logged in");
        waitAndRedirect('Webserver/Admin Panel/adminLogin.php',2);
      }
      
      printNavigationbar();
      //
    
      printScrpt();
      print('</body>');
      print('</html>');
?>

     