-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 13, 2013 at 03:52 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trainingdatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `achievements`
--

CREATE TABLE IF NOT EXISTS `achievements` (
  `Achievement_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Achievement_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Achievement_Bonus` int(11) NOT NULL,
  `Achievement_Type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Achievement_ID`),
  UNIQUE KEY `Achievement_Name` (`Achievement_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `achievement_content`
--

CREATE TABLE IF NOT EXISTS `achievement_content` (
  `Achievement_ID` int(11) NOT NULL,
  `Exercise_ID` int(11) NOT NULL,
  `Exercise_Count` int(11) NOT NULL,
  PRIMARY KEY (`Achievement_ID`,`Exercise_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `completed_achievements`
--

CREATE TABLE IF NOT EXISTS `completed_achievements` (
  `Trainee_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Achievement_ID` int(11) NOT NULL,
  PRIMARY KEY (`Trainee_ID`,`Achievement_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exercises`
--

CREATE TABLE IF NOT EXISTS `exercises` (
  `Exercise_ID` int(11) NOT NULL,
  `Exercise_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Exercise_ID`),
  UNIQUE KEY `Exercice_Name` (`Exercise_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `task_status`
--

CREATE TABLE IF NOT EXISTS `task_status` (
  `Training_Program_ID` int(11) NOT NULL,
  `Trainee_ID` int(11) NOT NULL,
  `Exercise_ID` int(11) NOT NULL,
  `Exercise_Count` int(11) NOT NULL,
  PRIMARY KEY (`Training_Program_ID`,`Trainee_ID`,`Exercise_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trainees`
--

CREATE TABLE IF NOT EXISTS `trainees` (
  `Trainee_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Trainee_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Trainee_Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Trainee_Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Trainee_Bonus` int(11) NOT NULL,
  PRIMARY KEY (`Trainee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE IF NOT EXISTS `trainers` (
  `Trainer_ID` int(8) NOT NULL AUTO_INCREMENT,
  `Trainer_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Trainer_Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Trainer_Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Trainer_Phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Trainer_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `training_program`
--

CREATE TABLE IF NOT EXISTS `training_program` (
  `Training_program_id` int(11) NOT NULL AUTO_INCREMENT,
  `Training_program_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Training_program_id`),
  UNIQUE KEY `training_program_name` (`Training_program_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `training_program_content`
--

CREATE TABLE IF NOT EXISTS `training_program_content` (
  `Training_Program_ID` int(11) NOT NULL,
  `Exercise_ID` int(11) NOT NULL,
  `Exercise_Count` int(11) NOT NULL,
  PRIMARY KEY (`Training_Program_ID`,`Exercise_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trains`
--

CREATE TABLE IF NOT EXISTS `trains` (
  `Trainer_ID` int(11) NOT NULL,
  `Trainee_ID` int(11) NOT NULL,
  `Training_Program_ID` int(11) NOT NULL,
  `Completed` int(2) NOT NULL,
  PRIMARY KEY (`Trainer_ID`,`Trainee_ID`,`Training_Program_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
